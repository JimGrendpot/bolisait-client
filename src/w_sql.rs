extern crate postgres;
use postgres::{Client, NoTls};
use postgres::row::Row;

pub fn connect_db() -> Client {
    Client::connect("postgres://site2:PEZD4@localhost/site2", NoTls).unwrap()
}

pub fn init_db(client: &mut Client) {
    client.batch_execute("
        CREATE TABLE news(
            id      SERIAL PRIMARY KEY, 
            name    TEXT NOT NULL,
            content TEXT
        );
        CREATE TABLE cat(
            id      SERIAL PRIMARY KEY, 
            name    TEXT NOT NULL,
            active  BOOLEAN
        );
        CREATE TABLE news_cat ( 
            id      SERIAL PRIMARY KEY, 
            news_id INTEGER, 
            cat_id  INTEGER, 
            active  BOOLEAN DEFAULT TRUE, 
            time    TIMESTAMP,
            FOREIGN KEY (news_id) REFERENCES news(id), 
            FOREIGN KEY (cat_id)  REFERENCES  cat(id)
        );
        ").unwrap();
}



pub fn kill_db(client: &mut Client) {
    client.batch_execute("DROP TABLE news_cat;").unwrap();
    client.batch_execute("DROP TABLE news;").unwrap();
    client.batch_execute("DROP TABLE cat;").unwrap();
}

#[test]
fn test_init_kill_db(){
    let mut con = connect_db();
    init_db(&mut con);
    kill_db(&mut con);
}

pub fn add_news(client: &mut Client, 
                name: &String, 
                content: &String) {
    client.execute("INSERT INTO news (name, content)
                    VALUES ($1, $2);", &[name, content]).unwrap();
}

pub fn news_len(client: &mut Client) -> i32 {
    let aaas: i64 = client.query("SELECT COUNT(*) FROM news;",&[]).unwrap()[0].get(0);
    aaas as i32
}

#[test]
fn test_add_news_db(){
    let mut con = connect_db();
    add_news(&mut con, 
        &mut "a".to_string(), 
        &mut "b".to_string());
}

pub fn add_ctgr(client: &mut Client, 
                name: &String) {
    client.execute("INSERT INTO cat (name)
                    VALUES ($1);", &[name]).unwrap();
}

pub fn list_cats(client: &mut Client)-> Vec<Row> {
    client.query("SELECT id, name, hidden FROM cat;", &[]).unwrap()
}

pub fn add_news_cat(client: &mut Client, 
                   news_id: &i32,
                   cat_id : &i32,
                   active : &bool) {
    client.execute("INSERT INTO news_cat (news_id, cat_id, active, time)
                    VALUES ($1, $2, $3, now());", &[news_id, cat_id, active]).unwrap();
}

pub fn id_ctgr(client: &mut Client,
               cat_name: &String) -> i32 {
    let aaas: i32 = client
        .query("SELECT id FROM cat WHERE name = $1;", &[cat_name])
        .unwrap()[0].get::<_, i32>(0);
    aaas as i32
}

pub fn update_content(client: &mut Client,
                     news_id: &i32,
                     content: &String) {
    client.execute("UPDATE news
                   SET content=$2
                   FROM news_cat
                   WHERE news.id = news_cat.news_id AND news_cat.id=$1;", &[news_id, content]).unwrap();
}
pub struct News {
    pub id     : i32,
    pub name   : String,
    pub content: String
}

pub fn hide_cat(client: &mut Client, id: &i32, hidden: &bool) {
    client.execute("UPDATE cat
                  SET hidden= $2
                  WHERE id = $1;", &[id, hidden]).unwrap();
}

pub fn get_news(client: &mut Client) -> Vec<Row> {
    client.query("SELECT id, name, content FROM news;", &[]).unwrap()
}

pub fn get_all_news_with_cat(client: &mut Client) -> Vec<Row> {
    client.query("SELECT news_cat.id, news.name, news.content, cat.name
                    FROM news_cat
                    INNER JOIN news ON news_cat.news_id = news.id
                    INNER JOIN cat  ON news_cat.cat_id  = cat.id
                    WHERE active = true;", &[]).unwrap()
}


pub fn news_get_by_id(client: &mut Client, id: &i32) -> Vec<Row> {
    client.query("SELECT news_cat.id, news.name, news.content, cat.name
                    FROM news_cat
                    INNER JOIN news ON news_cat.news_id = news.id
                    INNER JOIN cat  ON news_cat.cat_id  = cat.id
                    WHERE news.id = $1;", &[id]).unwrap()

}
pub fn del_nc_el(client: &mut Client, id_news: &i32) {
    client.execute("UPDATE news_cat SET active=false WHERE id=$1;", &[id_news]).unwrap();
    println!("Удолил {}", id_news);
}

pub fn up_time_news(client: &mut Client, id_news: &i32) {
    client.execute("UPDATE news_cat SET time=now() WHERE id=$1;", &[id_news]).unwrap();
    println!("Удолил {}", id_news);
}
#[test]
pub fn test_all() {
    let mut con = connect_db();
    add_news(&mut con, 
        &mut "a".to_string(), 
        &mut "b".to_string());

    kill_db(&mut con);
}

#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;

extern crate site1;
use std::env;
use rocket::request::{Form};
use rocket::response::{Flash, Redirect};
use rocket::http::RawStr;

use site1::w_sql;

use rocket_contrib::serve::StaticFiles;

extern crate rocket_contrib;
use rocket_contrib::templates::Template;

use postgres::row::Row;

#[derive(Serialize)]
struct Context {
    name: String,
    content: &'static str
}

#[derive(Serialize)]
pub struct MainNews {
    name: String,
    newss: Vec<Newsses>
}
#[derive(Serialize)]
pub struct MainCats {
    list: Vec<Cats>
}

#[derive(Serialize)]
pub struct Single {
    num: i32
}

#[derive(Serialize)]
pub struct EditableNews {
    id: i32,
    name: String,
    content: String,
    active: bool
}

#[derive(FromForm)]
struct Content { //for /content/news/<id> and /content/<id>
    name: String,
    content: String,
    ctgr: String,
    active: bool
}

#[derive(FromForm)]
struct Cat { //for /content/add_cat
    name: String,
}

#[derive(FromForm)]
pub struct Contents { //for /edit
    id_news: i32,
    content: String
}

#[derive(FromForm)]
struct NewsDel {
    id: i32
}

#[derive(Serialize)]
struct EditNewsCat{
    newss: Vec<Newsses>,
    cats: Vec<String>
}
#[derive(Serialize)]
pub struct Newsses {
    id  : i32,
    name: String,
    content: String
}
#[derive(Serialize)]
pub struct Cats {
    id  : i32,
    name: String,
    hidden: bool
}

// post reqests
#[post("/add_cat", data= "<content>")]
fn add_cat(content: Form<Cat>) -> Flash<Redirect>{
    println!("1");
    if (!content.name.is_empty()){
        let mut conn    = w_sql::connect_db();
        w_sql::add_ctgr(&mut conn, &content.name);
        let raw_content = w_sql::news_len(&mut conn);
        println!("add catgr {}", &content.name);
        Flash::success(Redirect::to("/content/cats"), "Successfully.")
    }
    else{
        Flash::success(Redirect::to("/content/cats"), "Error")
    }
}

#[post("/add_news", data= "<content>")]
fn add_news(content: Form<Content>) -> Flash<Redirect>{
    println!("1");
    if (!content.name.is_empty()) && (!content.content.is_empty()){
        let mut conn    = w_sql::connect_db();
        w_sql::add_news(&mut conn, 
                        &content.name, 
                        &content.content);
        println!("a");
        let raw_content = w_sql::news_len(&mut conn);
        let raw_id_cat  = w_sql::id_ctgr(&mut conn, &content.ctgr);
        w_sql::add_news_cat(&mut conn, 
                            &raw_content, 
                            &raw_id_cat, 
                            &content.active);
        Flash::success(Redirect::to("/content/add"), "Successfully.")
    }
    else{
        Flash::success(Redirect::to("/content/add"), "Error")
    }
}

#[post("/delnews", data="<content>")]
fn del_news(content: Form<NewsDel>) -> Flash<Redirect>{
    let mut conn = w_sql::connect_db();
    let id_news = content.id;
    w_sql::del_nc_el(&mut conn, &id_news);
    Flash::success(Redirect::to("/edit"), "Successfully.")
}

#[post("/unhidecat", data="<content>")]
fn unhide_cat(content: Form<NewsDel>) -> Flash<Redirect>{
    let mut conn = w_sql::connect_db();
    let id_cat  = content.id;
    w_sql::hide_cat(&mut conn, &id_cat, &true);
    Flash::success(Redirect::to("/content/cats"), "Successfully.")
}

#[post("/hidecat", data="<content>")]
fn hide_cat(content: Form<NewsDel>) -> Flash<Redirect>{
    let mut conn = w_sql::connect_db();
    let id_cat  = content.id;
    w_sql::hide_cat(&mut conn, &id_cat, &false);
    Flash::success(Redirect::to("/content/cats"), "Successfully.")
}

#[post("/uptime", data="<content>")]
fn up_news(content: Form<NewsDel>) -> Flash<Redirect>{
    let mut conn = w_sql::connect_db();
    let id_news = content.id;
    w_sql::up_time_news(&mut conn, &id_news);
    Flash::success(Redirect::to("/edit"), "Successfully.")
}

#[post("/editnews", data="<contents>")]
pub fn update_content_news(contents: Form<Contents>) -> Flash<Redirect>{
    let mut conn = w_sql::connect_db();
    let id_news  = contents.id_news;
    let content  = contents.content.to_string();
    println!("asd");
    w_sql::update_content(&mut conn, &id_news, &content);
    Flash::success(Redirect::to("/edit"), "Successfully.")
}

// get requests
// content
// Pages for create news 
#[get("/add")]
pub fn adding_news() -> Template{
    let context = Context { name: "Aaaa".to_string(),
                            content: "Ля"
                          };
    Template::render("new_news", &context)
}


#[get("/")]
pub fn index() -> Template {
    let emp = Single {
        num: 1312
    };
    Template::render("index", &emp)
}
#[get("/<name>")]
pub fn edit_news_cat(name: &RawStr) -> Template {
    let mut conn = w_sql::connect_db(); 	
    let newses: Vec<Row> = w_sql::get_all_news_with_cat(&mut conn); 	
    
    let mut con_ed = EditNewsCat{
        newss: Vec::new(),
        cats:  Vec::new()
    };

    for row in newses{
        con_ed.newss.push(Newsses{
            id      : row.get::<_, i32>(0),
            name    : row.get::<_, String>(1).to_string(),
            content : row.get::<_, String>(2).to_string()
        });
    }
    
    let cats: Vec<Row> = w_sql::list_cats(&mut conn);

    for row in cats{
        con_ed.cats.push(row.get::<_, String>(0).to_string());    
    }
    Template::render("news", &con_ed)
}

#[get("/edit")]
pub fn edit_news() -> Template{
    let mut conn = w_sql::connect_db(); 	
    let newses: Vec<Row> = w_sql::get_all_news_with_cat(&mut conn); 	
    
    let mut con_ed = EditNewsCat{
        newss: Vec::new(),
        cats:  Vec::new()
    };

    for row in newses{
        con_ed.newss.push(Newsses{
            id      : row.get::<_, i32>(0),
            name    : row.get::<_, String>(1).to_string(),
            content : row.get::<_, String>(2).to_string()
        });
    }
    
    let cats: Vec<Row> = w_sql::list_cats(&mut conn);

    for row in cats{
        con_ed.cats.push(row.get::<_, String>(1).to_string());    
    }
    Template::render("news", &con_ed)
}


#[get("/cats")]
pub fn news_ctgr() -> Template{
    let mut conn = w_sql::connect_db(); 	
    let mut cats_list = MainCats{ 		
        list: Vec::new()
    }; 		
    let cats: Vec<Row> = w_sql::list_cats(&mut conn);
    for row in cats {
        cats_list.list.push(Cats{
            id  : row.get::<_, i32>(0),
            name: row.get::<_, String>(1).to_string(),
            hidden: row.get::<_, bool>(2)
        })
    }
    Template::render("cat", &cats_list)
}

fn main() {
    rocket::ignite().mount("/", routes![        del_news,
                                                index,
                                                unhide_cat,
                                                hide_cat,
                                                edit_news,
                                                update_content_news,
                                                up_news]) 
                    .mount("/static", StaticFiles::from("./static"))
                    .mount("/content", routes![ 
                                                add_cat,
                                                add_news,
                                                news_ctgr,
                                                adding_news])
                    .mount("/edit/", routes![ edit_news_cat])
                    .mount("/cat", routes![])
                    .attach(Template::fairing())
                    .launch();
}
